;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                         ;;
;; Some global preferences ;;
;;                         ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;(add-to-list 'load-path "~/.emacs.d/elisp/")

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(add-to-list 'load-path "~/.emacs.d/elpa/")
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

(show-paren-mode 1)
(global-auto-revert-mode 1)

(setq default-directory (concat (getenv "HOME") "/")) ;;Set Home to always point to home

(column-number-mode t)
(line-number-mode t)
;;(setq-default indent-tabs-mode nil)                     ; Tabs, the root of all evil.
(setq large-file-warning-threshold 500000000)           ; Log files are often large.
(setq inhibit-startup-message t)                        ; Turn off welcome screen.
(setq split-height-threshold 1200)                      ; Prevent excessive splitting of windows.
(setq split-width-threshold  2000)                      ; Prevent excessive splitting of windows.
(setq backup-inhibited t)                               ; Disable backup, log files too big and intranet too slow.
(tool-bar-mode 0)                                       ; Disable the toolbar
(menu-bar-mode 0)                                       ; Disable menu bar
(scroll-bar-mode 0)                                     ; Disable scroll bar

;; Use clipboard for copy and paste
(setq x-select-enable-clipboard t)
;; Use which-function-mode
(which-function-mode 1)

;;;;;;;;;;;;;;;;;;;;
;;                ;;
;; My environment ;;
;;                ;;
;;;;;;;;;;;;;;;;;;;;

;;; Mac OS X Setup
(setq exec-path (append '("/opt/local/bin")
			exec-path))
(setq exec-path (append '("/usr/local/bin")
			exec-path))
;; key bindings
(when (eq system-type 'darwin) ;; mac specific settings
  (setq mac-option-modifier 'meta)
  (setq mac-command-modifier 'meta)
  (setq mac-right-option-modifier 'nil)
  (global-set-key [kp-delete] 'delete-char) ;; sets fn-delete to be right-delete
  )

(defun set-exec-path-from-shell-PATH ()
  "Set up Emacs' `exec-path' and PATH environment variable to match that used by the user's shell.

This is particularly useful under Mac OSX, where GUI apps are not started from a shell."
  (interactive)
  (let ((path-from-shell (replace-regexp-in-string "[ \t\n]*$" "" (shell-command-to-string "$SHELL --login -i -c 'echo $PATH'"))))
    (setenv "PATH" path-from-shell)
    (setq exec-path (split-string path-from-shell path-separator))))

;;With white background the default ANSI colors gets a bit too bright. Tune them down a bit.
(eval-after-load 'ansi-color
  '(progn
     (setq ansi-color-names-vector
           ["black" "#600" "#060" "#660"
            "#006" "#066" "#606" "white"])
     (setq ansi-color-map (ansi-color-make-color-map))))

;;Showing matching parentheses is nice. Having the whole expression within parens highlighted when cursor is on one of the parens is even nicer.
(show-paren-mode t)
(require 'paren)
(set-face-background 'show-paren-match (face-background 'default))
(set-face-foreground 'show-paren-match "#f0f0f0")
(set-face-attribute 'show-paren-match nil :weight 'extra-bold)
;;(set-face-background 'show-paren-mismatch-face "red")
;;(set-face-background 'show-paren-match-face "#f0f0f0")
;;(setq show-paren-style 'expression)

;; Set tango-dark as theme
(load-theme 'tango-dark t)

;; Hilight trailing whitespace
;; like this -->   
;;
(setq-default show-trailing-whitespace t)
(set-face-background 'trailing-whitespace "red")

;; Packages

(require 'package)

(setq package-archives '(("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")
			 ("gnu" . "http://elpa.gnu.org/packages/")))


'(require 'auto-complete-clang)
(load-file "./.emacs.d/elpa/xcscope-20180426.712/xcscope.el")
'(require 'xcscope)
;;(setq 'cscope-setup)
'(require 'flycheck)
;;(setq global-flycheck-mode)

;;Key bindings
(global-set-key "\C-x\C-b" 'buffer-menu)
(global-set-key "\M-g" 'goto-line) ;;I've never understood the default goto-line keybinding, so I've put goto-line directly on M-g.

(global-set-key [(control tab)] 'other-window)
(global-set-key [(control delete)] 'delete-region)

(define-key global-map [home] 'beginning-of-line)
(define-key global-map [end] 'end-of-line)

(define-key global-map [(control f2)]  'cscope-set-initial-directory)
(define-key global-map [(control f3)]  'cscope-unset-initial-directory)
;; Find references of a symbol
(define-key global-map [(control f5)]  'cscope-find-this-symbol)

;; Find definition of a symbol
(define-key global-map [(control f6)]  'cscope-find-global-definition)
(define-key global-map [(control f7)]  'cscope-find-global-definition-no-prompting)

;; Find related funtions (callers and callees)
(define-key global-map [(control f8)]  'cscope-pop-mark)
(define-key global-map [(control f9)]  'cscope-find-called-functions)
(define-key global-map [(control f10)] 'cscope-find-functions-calling-this-function)
(define-key global-map [(control f11)] 'cscope-display-buffer)
(define-key global-map [(control f12)] 'cscope-index-files)


;; Delete selected text
(delete-selection-mode t)
(ido-mode t)
(outline-minor-mode t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           Lazy lock          ;;
;; (to open large files faster) ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq font-lock-support-mode 'jit-lock-mode
      jit-lock-defer-time 0.01
      jit-lock-defer-contextually t
      jit-lock-stealth-time nil
      )

;; Flycheck
(add-hook 'python-mode-hook 'global-flycheck-mode)
(add-hook 'c-mode-hook 'global-flycheck-mode)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(custom-safe-themes
   (quote
    ("617219c11282b84761477059b9339da78ce392c974d9308535ee4ec8c0770bee" "94ba29363bfb7e06105f68d72b268f85981f7fba2ddef89331660033101eb5e5" default)))
 '(package-selected-packages
   (quote
    (flyspell-lazy paren-face magit auto-complete-clang flycheck xcscope)))
 '(show-paren-mode t))

;;(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 ;;'(default ((t (:family "DejaVu Sans Mono" :foundry "unknown" :slant normal :weight normal :height 98 :width normal)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
